package UtilsTest;

import org.junit.Assert;
import org.junit.Test;
import ro.wantsome.Utils;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class UtilsTest {
    @Test
    public void findPalindromesIdentifiesPalindrome(){
        Set<String>inputSet=new HashSet<>();
        inputSet.add("nitin");
        Set<String> palindromes = Utils.findPalindromes(inputSet);
        Assert.assertTrue(palindromes.contains("nitin"));
    }
    @Test
    public void findPalindromesFinder_FindsNothing(){
        Set<String>inputSet=new HashSet<>();
        inputSet.add("nitin");
        Set<String> palindromes = Utils.findPalindromes(inputSet);
        Assert.assertEquals(0,palindromes.size());
    }
    @Test
    public void findPalindromesFinder_FinderMultiples(){
        Set<String>inputSet=new HashSet<>();
        inputSet.add("nitin");
        inputSet.add("acasa");
        inputSet.add("casa");
        Set<String> palindromes = Utils.findPalindromes(inputSet);
        Assert.assertEquals(3,palindromes.size());
    }
    @Test
    public void findPalindromesFinder_IsResilient(){

        Set<String> palindromes = Utils.findPalindromes(null);
        Assert.assertEquals(0,palindromes.size());
    }
    @Test
    public void testRandomWord(){
        Set<String>inputSet=new HashSet<>();
        inputSet.add("1");
        inputSet.add("2");
        inputSet.add("3");
        String randomWord=Utils.getRandomWord(inputSet,new Random(1L));
        Assert.assertEquals(3,randomWord);

    }
    @Test
    public void test(){

    }
}
