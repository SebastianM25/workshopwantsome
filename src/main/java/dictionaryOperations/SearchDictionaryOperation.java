package dictionaryOperations;

import ro.wantsome.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class SearchDictionaryOperation implements DictionaryOperation {
    private BufferedReader in;
    private Set<String>wordSet;

    public SearchDictionaryOperation(Set<String> wordSet, BufferedReader in){
        this.in=in;
        this.wordSet=wordSet;
    }

    @Override
    public void run() throws IOException {
        System.out.println("Searching");
        String userGivenWord = in.readLine();


        Set<String> resultedWords = Utils.findWordsThatContain(wordSet, userGivenWord);
        //Sortare
        List<String> sortWords= Utils.palindromSort(resultedWords);
        Collections.sort(sortWords);

        for (String line : resultedWords) {
            System.out.println(line);
        }
    }
}
