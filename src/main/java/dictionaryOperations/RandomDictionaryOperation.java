package dictionaryOperations;

import ro.wantsome.Utils;

import java.io.IOException;
import java.util.List;
import java.util.Set;

public class RandomDictionaryOperation implements DictionaryOperation{
    private Set<String> wordSet;
    public RandomDictionaryOperation(Set<String> wordSet){
        this.wordSet=wordSet;
    }

    @Override
    public void run() throws IOException {

        Set<String> palindromes= Utils.findPalindromes(wordSet);
        List<String> sortedPalindromes=Utils.palindromSort(palindromes);
        for (String items:wordSet){
            System.out.println(items);
        }
    }
    //System.out.println("Palindromes");

}
