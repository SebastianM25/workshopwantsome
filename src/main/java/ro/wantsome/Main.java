package ro.wantsome;

import dictionaryOperations.DictionaryOperation;
import dictionaryOperations.RandomDictionaryOperation;
import dictionaryOperations.SearchDictionaryOperation;
import fileReader.InputFileReader;
import fileReader.ResourceInputFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Main {
    public static void main(String[] args) throws IOException {
        List<String> allines = new ArrayList<>();
        InputFileReader inputFileReader=new ResourceInputFile();
        allines = Utils.readAllLinesFromResourcesFile("dex.txt");

        //clean all that are  duplicate
        Set<String> wordSet = Utils.removeDuplicates(allines);
        //Scanner in = new Scanner(System.in);
        BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
        String userMeniuSelection=in.readLine();

        while (true) {
            System.out.println("Meniu: \n\t" +
                    "1. Search\n\t" +
                    "2. Palindromes\n\t" +
                    "3. Palindromes\n\t" +
                    "0. Exit");
            //int userSelection = in.nextInt();
            DictionaryOperation operation=null;
            switch (userMeniuSelection) {

                case "1":
                    operation=new SearchDictionaryOperation(wordSet,in);
                case "2":
                    //operation=new PalindromDictionaryOperation(wordSet);
                    break;
                case "3": operation=new RandomDictionaryOperation(wordSet);
                default:
                    System.out.println("Please select a valid option");

            }
            if (operation!=null){
                operation.run();
            }

        }
    }
}
