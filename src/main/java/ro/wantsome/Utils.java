package ro.wantsome;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Utils {
    /**
     * Can read a given file from the resources folder
     *
     * @param fileName file name
     * @return list of all the lines in the file
     */
    public static List<String> readAllLinesFromResourcesFile(String fileName) throws IOException {
        List<String> result = new ArrayList<>();
        ClassLoader classLoader = Main.class.getClassLoader();
        URL resource = classLoader.getResource(fileName);
        File file = null;
        if (resource == null) {
            throw new IllegalArgumentException("file not found!");
        } else {
            file = new File(resource.getFile());
        }

        try (FileReader reader = new FileReader(file);
             BufferedReader br = new BufferedReader(reader)) {

            String line;
            while ((line = br.readLine()) != null) {
                result.add(line);
            }
        }
        return result;
    }

    /**
     * Can read a given file in the working directory
     *
     * @param fileName file name
     * @return list of all the lines in the file
     */
    public static List<String> readAllLinesFromSourceFile(String fileName) throws IOException {
        return Files.readAllLines(Paths.get(fileName));
    }

    public static Set<String> removeDuplicates(List<String> allines) {
        Set<String> wordSet = new HashSet<>();
        for (String line : allines) {
            wordSet.add(line);
        }
        return wordSet;
    }

    public static Set<String> findWordsThatContain(Set<String> wordSet, String word) {
        Set<String> result = new HashSet<>();
        for (String line : wordSet) {
            if (line.contains(word))
                System.out.println(line);
        }
        return result;
    }

    public static Set<String> findPalindromes(Set<String> wordsList) {
        Set<String> result = new HashSet<>();
        if (wordsList == null) {
            System.out.println("Warning");
            return result;
        }
        for (String line : wordsList) {
            StringBuilder reverseword = new StringBuilder(line).reverse();
            if (line.equals(reverseword.toString()))
                result.add(line);
        }
        return result;
    }

    public static List<String> palindromSort(Set<String> unsorted) {
        List<String> result = new ArrayList<>();
        Collections.sort(result);
        return result;
    }

    public static String getRandomWord(Set<String> wordSet, Random random) {
        List<String> result = new ArrayList<>(wordSet);
        Random random = new Random();
        int index = random.nextInt(result.size());
        return result.get(index);


    }
}
